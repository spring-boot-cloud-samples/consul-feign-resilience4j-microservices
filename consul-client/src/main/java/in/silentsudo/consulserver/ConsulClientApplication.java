package in.silentsudo.consulserver;

import io.github.resilience4j.circuitbreaker.annotation.CircuitBreaker;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.availability.AvailabilityChangeEvent;
import org.springframework.boot.availability.ReadinessState;
import org.springframework.cloud.client.discovery.DiscoveryClient;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.stream.Collectors;

@EnableFeignClients
@SpringBootApplication
@EnableDiscoveryClient
public class ConsulClientApplication {
    public static void main(String[] args) {
        SpringApplication.run(ConsulClientApplication.class, args);
    }


    @RestController
    @RequestMapping("/")
    static class FeignController {
        private final ServerDataClient serverDataClient;
        private final DiscoveryClient discoveryClient;
        private final ApplicationEventPublisher publisher;

        FeignController(ServerDataClient serverDataClient,
                        DiscoveryClient discoveryClient,
                        ApplicationEventPublisher publisher) {
            this.serverDataClient = serverDataClient;
            this.discoveryClient = discoveryClient;
            this.publisher = publisher;
        }

        @GetMapping
        @CircuitBreaker(name = "consul-data-server", fallbackMethod = "fallback")
        public DataResponse dataResponse() {
            DataResponse data = serverDataClient.getData();
            List<String> servers = discoveryClient.getInstances("consul-data-server")
                    .stream()
                    .map(serviceInstance -> serviceInstance.getUri().toString())
                    .collect(Collectors.toList());
            data.setServers(servers);
            return data;
        }

        private DataResponse fallback(Throwable throwable) {
            return new DataResponse(null, null, null, throwable.getMessage());
        }

        @GetMapping("/stop")
        public String stop() {
            AvailabilityChangeEvent.publish(publisher, this, ReadinessState.REFUSING_TRAFFIC);
            return "Stopped";
        }

        @GetMapping("/start")
        public String start() {
            AvailabilityChangeEvent.publish(publisher, this, ReadinessState.ACCEPTING_TRAFFIC);
            return "Started";
        }
    }


}
