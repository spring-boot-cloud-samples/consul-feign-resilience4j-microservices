package in.silentsudo.consulserver;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

@JsonInclude(JsonInclude.Include.NON_NULL)
@AllArgsConstructor
@NoArgsConstructor
public class DataResponse {
    @Getter

    @Setter
    String server;
    @Getter
    @Setter
    String data;

    @Getter
    @Setter
    List<String> servers;

    @Getter
    @Setter
    private String error;

}