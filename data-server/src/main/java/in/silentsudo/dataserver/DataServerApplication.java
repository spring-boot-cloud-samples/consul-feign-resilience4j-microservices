package in.silentsudo.dataserver;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.core.env.Environment;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@EnableDiscoveryClient
@SpringBootApplication
public class DataServerApplication {
    public static void main(String[] args) {
        SpringApplication.run(DataServerApplication.class, args);
    }

    @RestController
    @RequestMapping("/data")
    static class SampeDataController {


        private final Environment environment;

        SampeDataController(Environment environment) {
            this.environment = environment;
        }

        @GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
        public Map<String, String> getData() {
            return Stream.of(new String[][]{
                    {"data", "server application data"},
                    {"server", environment.getProperty("local.server.port")}
            }).collect(Collectors.toMap(entry -> entry[0], entry -> entry[1]));
        }
    }
}
